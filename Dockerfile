from madduci/docker-cpp-env:latest

RUN apk add --update git
WORKDIR /ninjam
RUN git clone https://www-dev.cockos.com/ninjam/ninjam.git /ninjam
WORKDIR /ninjam/ninjam/server/
RUN make
COPY ninjam.cfg /ninjam/ninjam/server/

EXPOSE 2049
ENTRYPOINT ["/ninjam/ninjam/server/ninjamsrv", "ninjam.cfg"]
